## Development setup

1. Specify a PostgreSQL database URL along with a username and password in `src/main/resources/application.properties`.
2. (optional) If you don't want to load sample data upon the initial run, delete the following file: `src/main/java/com/dragnev/keyblogger/applicationEvent/DbContextRefreshListener.java`.
3. Run with your favorite IDE

## Screenshots

<img src="screen-1.png" alt="Screenshot 1" style="max-width:500px;max-height:500px;">

<img src="screen-2.png" alt="Screenshot 2" style="max-width:500px;max-height:500px;">

<img src="screen-3.png" alt="Screenshot 3" style="max-width:500px;max-height:500px;">

<img src="screen-4.png" alt="Screenshot 4" style="max-width:500px;max-height:500px;">

<img src="screen-5.png" alt="Screenshot 5" style="max-width:500px;max-height:500px;">

<img src="screen-6.png" alt="Screenshot 6" style="max-width:500px;max-height:500px;">

<img src="screen-7.png" alt="Screenshot 7" style="max-width:500px;max-height:500px;">

<img src="screen-8.png" alt="Screenshot 8" style="max-width:500px;max-height:500px;">