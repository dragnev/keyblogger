package com.dragnev.keyblogger.service;

import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.dragnev.keyblogger.entity.User;
import com.dragnev.keyblogger.repository.UserRepository;

public class BlogUserDetailsServiceTest {
    @Test
    void loadUserByUsername_ValidEmail_ReturnsUserDetails() {
        // Arrange
        UserRepository userRepository = mock(UserRepository.class);
        User user = new User("test@example.com", "Test Test", "password");
        when(userRepository.findByEmail(eq("test@example.com"))).thenReturn(user);

        BlogUserDetailsService service = new BlogUserDetailsService(userRepository);

        // Act
        UserDetails userDetails = service.loadUserByUsername("test@example.com");

        // Assert
        assertNotNull(userDetails);
        assertEquals("test@example.com", userDetails.getUsername());
        // Perform additional assertions as needed
    }

    @Test
    void loadUserByUsername_InvalidEmail_ThrowsException() {
        // Arrange
        UserRepository userRepository = mock(UserRepository.class);
        when(userRepository.findByEmail(eq("test@example.com"))).thenReturn(null);

        BlogUserDetailsService service = new BlogUserDetailsService(userRepository);

        // Act and Assert
        assertThrows(UsernameNotFoundException.class, () -> {
            service.loadUserByUsername("test@example.com");
        });
    }
}
