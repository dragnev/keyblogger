package com.dragnev.keyblogger.controller.rest;
import static io.restassured.RestAssured.*;

import com.dragnev.keyblogger.bindingModel.ArticleBindingModel;
import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.User;
import com.dragnev.keyblogger.entity.Category;
import com.dragnev.keyblogger.repository.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class ArticleRestControllerTests {
    @MockBean
    private ArticleRepository articleRepository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private TagRepository tagRepository;
    @Test
    public void testCreateProcess_ValidArticle_ReturnsOk() {
        // Mock repository behavior
        User user = new User("test@example.com", "name", "password");
        Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(user);
        Mockito.when(categoryRepository.getById(Mockito.anyInt())).thenReturn(new Category());
        Mockito.when(tagRepository.findByName(Mockito.anyString())).thenReturn(null);

        ArticleBindingModel articleBindingModel = new ArticleBindingModel();
        articleBindingModel.setTitle("Test Article");
        articleBindingModel.setContent("This is a test article");
        articleBindingModel.setCategoryId(1);
        articleBindingModel.setTagString("tag1, tag2");

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(articleBindingModel)
                .when().post("/api/articles/create")
                .then().statusCode(200);
    }

    @Test
    public void testCreateProcess_InvalidArticleData_ReturnsBadRequest() {
        // Mock repository behavior
        User user = new User("test@example.com", "name", "password");
        Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(user);

        ArticleBindingModel articleBindingModel = new ArticleBindingModel();

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(articleBindingModel)
                .when().post("/api/articles/create")
                .then().statusCode(400);
    }

    @Test
    public void testCreateProcess_UnauthorizedUser_ReturnsBadRequest() {
        // Mock repository behavior
        Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(null);

        ArticleBindingModel articleBindingModel = new ArticleBindingModel();

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(articleBindingModel)
                .when().post("/api/articles/create")
                .then().statusCode(400);
    }

    @Test
    public void testEditProcess_ValidArticle_ReturnsOk() {
        // Mock repository behavior
        Article existingArticle = new Article();
        Mockito.when(articleRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(articleRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(existingArticle));
        Mockito.when(categoryRepository.getById(Mockito.anyInt())).thenReturn(new Category());
        Mockito.when(tagRepository.findByName(Mockito.anyString())).thenReturn(null);

        ArticleBindingModel articleBindingModel = new ArticleBindingModel();
        articleBindingModel.setTitle("Updated Article Title");
        articleBindingModel.setContent("Updated article content");
        articleBindingModel.setCategoryId(1);
        articleBindingModel.setTagString("tag1, tag2");

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(articleBindingModel)
                .when().put("/api/articles/edit/{id}", 1)
                .then().statusCode(200);
    }

    @Test
    public void testEditProcess_InvalidArticleId_ReturnsBadRequest() {
        // Mock repository behavior
        Mockito.when(articleRepository.existsById(Mockito.anyInt())).thenReturn(false);

        ArticleBindingModel articleBindingModel = new ArticleBindingModel();

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(articleBindingModel)
                .when().put("/api/articles/edit/{id}", 1)
                .then().statusCode(400);
    }

    @Test
    public void testEditProcess_UnauthorizedUser_ReturnsBadRequest() {
        // Mock repository behavior
        Article existingArticle = new Article();
        Mockito.when(articleRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(articleRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(existingArticle));

        ArticleBindingModel articleBindingModel = new ArticleBindingModel();

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(articleBindingModel)
                .when().put("/api/articles/edit/{id}", 1)
                .then().statusCode(400);
    }

    @Test
    public void testEditProcess_InvalidArticleData_ReturnsBadRequest() {
        // Mock repository behavior
        Article existingArticle = new Article();
        Mockito.when(articleRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(articleRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(existingArticle));
        Mockito.when(categoryRepository.getById(Mockito.anyInt())).thenReturn(new Category());
        Mockito.when(tagRepository.findByName(Mockito.anyString())).thenReturn(null);

        ArticleBindingModel articleBindingModel = new ArticleBindingModel();

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(articleBindingModel)
                .when().put("/api/articles/edit/{id}", 1)
                .then().statusCode(400);
    }
}
