package com.dragnev.keyblogger.controller.rest;

import com.dragnev.keyblogger.bindingModel.CommentBindingModel;
import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.Comment;
import com.dragnev.keyblogger.entity.User;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CommentRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import java.util.Arrays;
import java.util.Collections;

import static io.restassured.RestAssured.given;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class CommentRestControllerTests {
    @MockBean
    private CommentRepository commentRepository;

    @MockBean
    private ArticleRepository articleRepository;

    @MockBean
    private UserRepository userRepository;
    @Test
    public void testGetByArticle() {
        // Mock repository behavior
        Article article = new Article();
        Mockito.when(articleRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(articleRepository.getById(Mockito.anyInt())).thenReturn(article);
        Mockito.when(commentRepository.findByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc(Mockito.any()))
                .thenReturn(Arrays.asList(new Comment()));

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/comments/article/{id}", 1)
                .then().statusCode(200);
    }

    @Test
    public void testGetChildrenByParentComment_ValidId_ReturnsComments() {
        // Mock repository behavior
        Comment parentComment = new Comment();
        parentComment.setArticle(new Article());
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(parentComment);
        Mockito.when(commentRepository.findByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc(Mockito.any()))
                .thenReturn(Arrays.asList(new Comment()));

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/comments/{id}/childComments", 1)
                .then().statusCode(200);
    }

    @Test
    public void testGetChildrenByParentComment_InvalidId_ReturnsBadRequest() {
        // Mock repository behavior
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(false);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/comments/{id}/childComments", 1)
                .then().statusCode(400);
    }

    @Test
    public void testGetChildrenByParentComment_NoAssociatedArticle_ReturnsBadRequest() {
        // Mock repository behavior
        Comment parentComment = new Comment();
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(parentComment);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/comments/{id}/childComments", 1)
                .then().statusCode(400);
    }

    @Test
    public void testGetChildrenByParentComment_EmptyChildComments_ReturnsEmptyList() {
        // Mock repository behavior
        Comment parentComment = new Comment();
        parentComment.setArticle(new Article());
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(parentComment);
        Mockito.when(commentRepository.findByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc(Mockito.any()))
                .thenReturn(Collections.emptyList());

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/comments/{id}/childComments", 1)
                .then().statusCode(200)
//                .body("size()", is(0))
        ;
    }

    @Test
    public void testCreateProcess_ValidComment_ReturnsCreatedComment() {
        // Mock repository behavior
        CommentBindingModel commentBindingModel = new CommentBindingModel();
        commentBindingModel.setText("Test comment");
        commentBindingModel.setArticleId(1);

        User user = new User("test email", "test name", "test password");
        Mockito.when(userRepository.findByEmail(Mockito.any())).thenReturn(user);
        Mockito.when(articleRepository.existsById(Mockito.anyInt())).thenReturn(true);

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(commentBindingModel)
                .when().post("/")
                .then().statusCode(200);
    }

    @Test
    public void testCreateProcess_InvalidCommentData_ReturnsBadRequest() {
        // Perform the request without comment data and validate the response
        given().contentType("application/json")
                .when().post("/")
                .then().statusCode(400);
    }

    @Test
    public void testCreateProcess_InvalidArticleId_ReturnsBadRequest() {
        // Mock repository behavior
        CommentBindingModel commentBindingModel = new CommentBindingModel();
        commentBindingModel.setText("Test comment");
        commentBindingModel.setArticleId(1);
        Mockito.when(articleRepository.existsById(Mockito.anyInt())).thenReturn(false);

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(commentBindingModel)
                .when().post("/")
                .then().statusCode(400);
    }

    @Test
    public void testCreateProcess_InvalidParentCommentId_ReturnsBadRequest() {
        // Mock repository behavior
        CommentBindingModel commentBindingModel = new CommentBindingModel();
        commentBindingModel.setText("Test comment");
        commentBindingModel.setParentCommentId(1);
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(false);

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(commentBindingModel)
                .when().post("/")
                .then().statusCode(400);
    }

    @Test
    public void testCreateProcess_UnauthorizedUser_ReturnsUnauthorized() {
        // Mock repository behavior
        CommentBindingModel commentBindingModel = new CommentBindingModel();
        commentBindingModel.setText("Test comment");
        commentBindingModel.setArticleId(1);
        Mockito.when(userRepository.findByEmail(Mockito.any())).thenReturn(null);

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(commentBindingModel)
                .when().post("/")
                .then().statusCode(401);
    }

    @Test
    public void testEditProcess_ValidComment_ReturnsEditedComment() {
        // Mock repository behavior
        Comment comment = new Comment();
        comment.setText("Initial comment");
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(comment);
        Mockito.when(commentRepository.saveAndFlush(Mockito.any())).thenReturn(comment);

        CommentBindingModel commentBindingModel = new CommentBindingModel();
        commentBindingModel.setText("Edited comment");

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(commentBindingModel)
                .when().put("/edit/{id}", 1)
                .then().statusCode(200);
    }
    @Test
    public void testEditProcess_InvalidCommentId_ReturnsBadRequest() {
        // Mock repository behavior
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(false);

        CommentBindingModel commentBindingModel = new CommentBindingModel();
        commentBindingModel.setText("Edited comment");

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(commentBindingModel)
                .when().put("/edit/{id}", 1)
                .then().statusCode(400);
    }

    @Test
    public void testEditProcess_EmptyCommentText_ReturnsBadRequest() {
        // Mock repository behavior
        Comment comment = new Comment();
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(comment);

        CommentBindingModel commentBindingModel = new CommentBindingModel();
        commentBindingModel.setText("");

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(commentBindingModel)
                .when().put("/edit/{id}", 1)
                .then().statusCode(400);
    }

    @Test
    public void testEditProcess_UnauthorizedUser_ReturnsBadRequest() {
        // Mock repository behavior
        Comment comment = new Comment();
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(comment);

        CommentBindingModel commentBindingModel = new CommentBindingModel();
        commentBindingModel.setText("Edited comment");

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(commentBindingModel)
                .when().put("/edit/{id}", 1)
                .then().statusCode(400);
    }

    @Test
    public void testDeleteProcess_ValidComment_ReturnsOk() {
        // Mock repository behavior
        Comment comment = new Comment();
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(comment);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().delete("/delete/{id}", 1)
                .then().statusCode(200);
    }

    @Test
    public void testDeleteProcess_InvalidCommentId_ReturnsBadRequest() {
        // Mock repository behavior
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(false);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().delete("/delete/{id}", 1)
                .then().statusCode(400);
    }

    @Test
    public void testDeleteProcess_UnauthorizedUser_ReturnsBadRequest() {
        // Mock repository behavior
        Comment comment = new Comment();
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(comment);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().delete("/delete/{id}", 1)
                .then().statusCode(400);
    }
}
