package com.dragnev.keyblogger.controller.admin.rest;

import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.Comment;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CommentRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static io.restassured.RestAssured.given;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class AdminCommentRestControllerTests {
    @MockBean
    private CommentRepository commentRepository;

    @MockBean
    private ArticleRepository articleRepository;

    @MockBean
    private UserRepository userRepository;

    @Test
    public void testGetByArticle() {
        // Mock repository behavior
        Article article = new Article();
        Mockito.when(articleRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(articleRepository.getById(Mockito.anyInt())).thenReturn(article);
        Mockito.when(commentRepository.findByArticleOrderByCreatedDateDesc(Mockito.any()))
                .thenReturn(Collections.singletonList(new Comment()));

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/admin/comments/article/{id}", 1)
                .then().statusCode(200);
    }

    @Test
    public void testGetChildrenByParentComment() {
        // Mock repository behavior
        Comment parentComment = new Comment();
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(parentComment);
        Mockito.when(commentRepository.findByParentCommentOrderByCreatedDateAsc(Mockito.any()))
                .thenReturn(Collections.singletonList(new Comment()));

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/admin/comments/{id}/childComments", 1)
                .then().statusCode(200);
    }

    @Test
    public void testGetChildrenByParentComment_NonExistentParentComment() {
        // Mock repository behavior for non-existent parent comment
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(false);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/admin/comments/{id}/childComments", 1)
                .then().statusCode(400);
    }

    @Test
    public void testGetChildrenByParentComment_NotTopLevelComment() {
        // Mock repository behavior for non-top-level comment
        Comment parentComment = new Comment();
        parentComment.setArticle(null); // Set article to null to simulate non-top-level comment
        Mockito.when(commentRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(parentComment);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/admin/comments/{id}/childComments", 1)
                .then().statusCode(400);
    }
}
