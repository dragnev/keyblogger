package com.dragnev.keyblogger.controller.admin.rest;

import com.dragnev.keyblogger.entity.Category;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class CategoryRestControllerTests {

    @MockBean
    private CategoryRepository categoryRepository;

    @MockBean
    private ArticleRepository articleRepository;
    @Test
    public void testGet() {
        // Mock repository behavior
        List<Category> categories = new ArrayList<>();
        categories.add(new Category("Category 1"));
        categories.add(new Category("Category 2"));
        Mockito.when(categoryRepository.findAll()).thenReturn(categories);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().get("/api/")
                .then().statusCode(200);
    }
}
