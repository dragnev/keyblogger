package com.dragnev.keyblogger.controller.admin.rest;
import com.dragnev.keyblogger.bindingModel.UserEditBindingModel;
import com.dragnev.keyblogger.entity.User;
import com.dragnev.keyblogger.entity.Role;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.RoleRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

import static io.restassured.RestAssured.given;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class AdminUserRestControllerTests {
    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ArticleRepository articleRepository;

    @MockBean
    private RoleRepository roleRepository;
    @Test
    public void testEditProcess() {
        // Mock repository behavior
        User user = new User();
        Mockito.when(userRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(user);
        Mockito.when(roleRepository.getById(Mockito.anyInt())).thenReturn(new Role());

        // Prepare the request body
        UserEditBindingModel userEditBindingModel = new UserEditBindingModel();
        userEditBindingModel.setFullName("John Doe");
        userEditBindingModel.setEmail("john.doe@example.com");
        userEditBindingModel.setPassword("newpassword");
        userEditBindingModel.setConfirmPassword("newpassword");
        userEditBindingModel.setRoles(Collections.singletonList(1)); // Assuming role ID 1 exists

        // Perform the request and validate the response
        given().contentType("application/json")
                .body(userEditBindingModel)
                .when().put("/api/edit/{id}", 1)
                .then().statusCode(200);
    }

    @Test
    public void testDeleteTest() {
        // Mock repository behavior
        User user = new User();
        Mockito.when(userRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(user);

        // Perform the request and validate the response
        given().contentType("application/json")
                .when().delete("/api/delete/{id}", 1)
                .then().statusCode(200);
    }
}
