package com.dragnev.keyblogger.repository;

import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.Category;
import com.dragnev.keyblogger.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class UserRepositoryTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void testFindByArticlesIsNotEmpty() {
        // Create users with articles
        User user1 = new User("test1@example.com", "Test User 1", "password");
        User user2 = new User("test2@example.com", "Test User 2", "password");

        Category category = new Category("Test Category");
        Article article1 = new Article("Test Article 1", "Test Content 1", user1, category);
        Article article2 = new Article("Test Article 2", "Test Content 2", user2, category);

        entityManager.persist(user1);
        entityManager.persist(user2);
        entityManager.persist(category);
        entityManager.persist(article1);
        entityManager.persist(article2);

        List<User> users = userRepository.findByArticlesIsNotEmpty();
        assertEquals(2, users.size());
    }

    @Test
    public void testFindByArticlesIsNotEmpty_EmptyArticles() {// Create users without articles
        User user1 = new User("test1@example.com", "Test User 1", "password");
        User user2 = new User("test2@example.com", "Test User 2", "password");

        entityManager.persist(user1);
        entityManager.persist(user2);

        List<User> users = userRepository.findByArticlesIsNotEmpty();
        assertEquals(0, users.size());
    }

    @Test
    public void testFindByArticlesIsNotEmpty_EmptyAndNonEmptyArticles() {
        // Create users with and without articles
        User user1 = new User("test1@example.com", "Test User 1", "password");
        User user2 = new User("test2@example.com", "Test User 2", "password");

        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user1, category);

        entityManager.persist(user1);
        entityManager.persist(user2);
        entityManager.persist(category);
        entityManager.persist(article);

        List<User> users = userRepository.findByArticlesIsNotEmpty();
        assertEquals(1, users.size());
        assertEquals(user1.getId(), users.get(0).getId());
    }
}
