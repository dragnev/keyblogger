package com.dragnev.keyblogger.repository;
import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.Category;
import com.dragnev.keyblogger.entity.Comment;
import com.dragnev.keyblogger.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ActiveProfiles("test")
public class CommentRepositoryTests {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void testFindByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc() {
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, "Comment 2");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc(article);

        // Perform assertions
        assertEquals(2, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals("Comment 1", comments.get(1).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(0, comments.get(1).getChildComments().size());
        assertNull(comments.get(1).getParentComment());
        assertNull(comments.get(0).getParentComment());
        assertTrue(comments.get(1).getCreatedDate().before(comments.get(0).getCreatedDate()));
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
        assertEquals(article.getId(), comments.get(1).getArticle().getId());
    }
    @Test
    public void testFindByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc_ExcludeHidden() {

        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, "Comment 2");
        comment2.setIsHidden(true);

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc(article);

        // Perform assertions
        assertEquals(1, comments.size());
        assertEquals("Comment 1", comments.get(0).getText());
    }

    @Test
    public void testFindByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc_NoComments() {
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc(article);

        // Perform assertions
        assertEquals(0, comments.size());
    }

    @Test
    public void testFindByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc_OneComment() {

        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc(article);

        // Perform assertions
        assertEquals(1, comments.size());
        assertEquals("Comment 1", comments.get(0).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertNull(comments.get(0).getParentComment());
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
    }

    @Test
    public void testFindByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc() {
        // Regular scenario with multiple comments: Test the method with a parent comment that has multiple child comments.
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, comment1, "Comment 2");
        Comment comment3 = new Comment(user, article, comment1, "Comment 3");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);
        entityManager.persist(comment3);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc(comment1);

        // Perform assertions
        assertEquals(2, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals("Comment 3", comments.get(1).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(0, comments.get(1).getChildComments().size());
        assertEquals(comment1, comments.get(0).getParentComment());
        assertEquals(comment1, comments.get(1).getParentComment());
        assertTrue(comments.get(0).getCreatedDate().before(comments.get(1).getCreatedDate()));
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
        assertEquals(article.getId(), comments.get(1).getArticle().getId());
    }

    @Test
    public void testFindByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc_NoChildren() {
        // Regular scenario with no comments: Test the method with a parent comment that has no child comments. Assert that the returned list is empty.

        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc(comment1);

        // Perform assertions
        assertEquals(0, comments.size());
    }

    @Test
    public void testFindByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc_OneChildren() {
        // Edge case: Parent comment with only one child comment: Test the method with a parent comment that has only one child comment. Verify that the returned list contains the single child comment and that the order is correct.
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, comment1, "Comment 2");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc(comment1);

        // Perform assertions
        assertEquals(1, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(comment1, comments.get(0).getParentComment());
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
    }

    @Test
    public void testFindByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc_ExcludeHidden() {
        // Edge case: Parent comment with hidden child comments: Test the scenario where the parent comment has child comments, but some of them are hidden. Verify that the returned list only includes non-hidden child comments and that they are ordered correctly.
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, comment1, "Comment 2");
        Comment comment3 = new Comment(user, article, comment1, "Comment 3");
        Comment comment4 = new Comment(user, article, comment1, "Comment 4");
        comment4.setIsHidden(true);

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);
        entityManager.persist(comment3);
        entityManager.persist(comment4);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc(comment1);

        // Perform assertions
        assertEquals(2, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals("Comment 3", comments.get(1).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(0, comments.get(1).getChildComments().size());
        assertEquals(comment1, comments.get(0).getParentComment());
        assertEquals(comment1, comments.get(1).getParentComment());
        assertTrue(comments.get(0).getCreatedDate().before(comments.get(1).getCreatedDate()));
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
        assertEquals(article.getId(), comments.get(1).getArticle().getId());
    }

    @Test
    public void testFindByArticleOrderByCreatedDateDescTest() {
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, "Comment 2");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByArticleOrderByCreatedDateDesc(article);

        // Perform assertions
        assertEquals(2, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals("Comment 1", comments.get(1).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(0, comments.get(1).getChildComments().size());
        assertNull(comments.get(1).getParentComment());
        assertNull(comments.get(0).getParentComment());
        assertTrue(comments.get(1).getCreatedDate().before(comments.get(0).getCreatedDate()));
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
        assertEquals(article.getId(), comments.get(1).getArticle().getId());
    }

    @Test
    public void testFindByArticleOrderByCreatedDateDesc_IncludeHidden() {
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, "Comment 2");
        comment2.setIsHidden(true);

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByArticleOrderByCreatedDateDesc(article);

        // Perform assertions
        assertEquals(2, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals("Comment 1", comments.get(1).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(0, comments.get(1).getChildComments().size());
        assertNull(comments.get(1).getParentComment());
        assertNull(comments.get(0).getParentComment());
        assertTrue(comments.get(1).getCreatedDate().before(comments.get(0).getCreatedDate()));
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
        assertEquals(article.getId(), comments.get(1).getArticle().getId());
    }


    @Test
    public void testFindByArticleOrderByCreatedDateDesc_NoComments() {
        // Edge case: No comments matching the criteria
        // Create test data
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByArticleOrderByCreatedDateDesc(article);

        // Perform assertions
        assertEquals(0, comments.size());
    }

    @Test
    public void testFindByArticleOrderByCreatedDateDesc_OneComment() {
        // Edge case: Only one comment matching the criteria
        // Create test data
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByArticleOrderByCreatedDateDesc(article);

        // Perform assertions
        assertEquals(1, comments.size());
        assertEquals("Comment 1", comments.get(0).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertNull(comments.get(0).getParentComment());
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
    }

    @Test
    public void testFindByParentCommentOrderByCreatedDateAsc() {
        // Regular scenario with multiple comments: Test the method with a parent comment that has multiple child comments.
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, comment1, "Comment 2");
        Comment comment3 = new Comment(user, article, comment1, "Comment 3");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);
        entityManager.persist(comment3);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByParentCommentOrderByCreatedDateAsc(comment1);

        // Perform assertions
        assertEquals(2, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals("Comment 3", comments.get(1).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(0, comments.get(1).getChildComments().size());
        assertEquals(comment1, comments.get(0).getParentComment());
        assertEquals(comment1, comments.get(1).getParentComment());
        assertTrue(comments.get(0).getCreatedDate().before(comments.get(1).getCreatedDate()));
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
        assertEquals(article.getId(), comments.get(1).getArticle().getId());
    }

    @Test
    public void testFindByParentCommentOrderByCreatedDateAsc_NoChildren() {
        // Regular scenario with no comments: Test the method with a parent comment that has no child comments. Assert that the returned list is empty.
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByParentCommentOrderByCreatedDateAsc(comment1);

        // Perform assertions
        assertEquals(0, comments.size());
    }

    @Test
    public void testFindByParentCommentOrderByCreatedDateAsc_OneChildren() {
        // Edge case: Parent comment with only one child comment: Test the method with a parent comment that has only one child comment. Verify that the returned list contains the single child comment and that the order is correct.
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, comment1, "Comment 2");

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByParentCommentOrderByCreatedDateAsc(comment1);

        // Perform assertions
        assertEquals(1, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(comment1, comments.get(0).getParentComment());
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
    }

    @Test
    public void testFindByParentCommentOrderByCreatedDateAsc_IncludeHidden() {
        // Edge case: Parent comment with hidden child comments: Test the scenario where the parent comment has child comments, but some of them are hidden. Verify that the returned list only includes non-hidden child comments and that they are ordered correctly.
        User user = new User("test@example.com", "Test Test", "password");
        Category category = new Category("Test Category");
        Article article = new Article("Test Article", "Test Content", user, category);
        Comment comment1 = new Comment(user, article, "Comment 1");
        Comment comment2 = new Comment(user, article, comment1, "Comment 2");
        Comment comment3 = new Comment(user, article, comment1, "Comment 3");
        Comment comment4 = new Comment(user, article, comment1, "Comment 4");
        comment4.setIsHidden(true);

        // Persist the entities
        entityManager.persist(user);
        entityManager.persist(category);
        entityManager.persist(article);
        entityManager.persist(comment1);
        entityManager.persist(comment2);
        entityManager.persist(comment3);
        entityManager.persist(comment4);

        // Call the repository method to be tested
        List<Comment> comments = commentRepository.findByParentCommentOrderByCreatedDateAsc(comment1);

        // Perform assertions
        assertEquals(3, comments.size());
        assertEquals("Comment 2", comments.get(0).getText());
        assertEquals("Comment 3", comments.get(1).getText());
        assertEquals("Comment 4", comments.get(2).getText());
        assertEquals(0, comments.get(0).getChildComments().size());
        assertEquals(0, comments.get(1).getChildComments().size());
        assertEquals(0, comments.get(2).getChildComments().size());
        assertEquals(comment1, comments.get(0).getParentComment());
        assertEquals(comment1, comments.get(1).getParentComment());
        assertEquals(comment1, comments.get(2).getParentComment());
        assertTrue(comments.get(0).getCreatedDate().before(comments.get(1).getCreatedDate()));
        assertTrue(comments.get(1).getCreatedDate().before(comments.get(2).getCreatedDate()));
        assertEquals(article.getId(), comments.get(0).getArticle().getId());
        assertEquals(article.getId(), comments.get(1).getArticle().getId());
        assertEquals(article.getId(), comments.get(2).getArticle().getId());
    }
}