// generic functionality for the AJAX modals in the admin area

const token = document.querySelector('meta[name="_csrf"]').getAttribute('content');
const header = document.querySelector('meta[name="_csrf_header"]').getAttribute('content');
let modal = document.getElementById('actionsModal');
let modalHeading = modal.querySelector("#modal-heading");
// var because these are referenced in the view-specific files' methods
var modalForm = modal.querySelector("#actions-form");
var saveButton = modal.querySelector('#saveButton');
var deleteButton = modal.querySelector('#deleteButton');
let formAlertElement = modalForm.querySelector('div[data-abide-error]');

function buttonLoadingState(loading, button, label) {
    button.style = `padding: ${loading ? 0.35 : 0.85}em 1em;`;
    button.innerHTML = loading ? '<img style="height: 30px;width: 30px" src="/svg/loader.svg"> Loading' : label;
}

function addMessageAndRedirect(button) {
    button.innerHTML = '<img style="height: 30px;width: 30px" src="/svg/check.svg"> Redirecting';
    window.location.href = redirect;
}

function toggleFormError(enabled, message) {
    formAlertElement.style.display = enabled ? 'inherit' : 'none';
    formAlertElement.firstElementChild.innerHTML = message;
}

function overrideDefaultSubmit(event) { event.preventDefault(); }

function arrangeModal(actionLabel, itemType, editable, data, request) {
    let button = editable ? saveButton : deleteButton;
    modalHeading.innerHTML = `${actionLabel} ${itemType}`;
    toggleEditableState(editable, actionLabel);
    button.onclick = function () {
    // $('#actions-form').on('valid.fndtn.abide', function() {
        let xhr = new XMLHttpRequest();
        xhr.open(request.type, request.path, true);

        // Send the proper header information along with the request
        xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
        xhr.setRequestHeader(header, token);
        buttonLoadingState(true, button, null);
        // TODO: prevent double submitting
        // TODO: don't allow closing the modal until a response

        xhr.onreadystatechange = function () { // Call a function when the state changes.
            // this.readyState === XMLHttpRequest.DONE &&
            if (this.status === 200) {
                // Request finished. Do processing here.
                addMessageAndRedirect(button);
            } else {
                // On error
                buttonLoadingState(false, button, actionLabel);
                toggleFormError(true, "Bad response from the server.");
            }
        }

        if (editable) {
            let item = bindDataToObject(data);

            xhr.send(JSON.stringify(item));
        }
        else
            xhr.send();
    }
    $('#actionsModal').foundation('open');
}