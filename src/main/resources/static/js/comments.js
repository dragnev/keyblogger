var token = document.querySelector('meta[name="_csrf"]').getAttribute('content');

let commentSection = document.getElementById("comment-section");
let commentTemplate = document.getElementById('media-template');
let commentBox = document.getElementById('comment-box');
let commentsContainer = document.getElementById('user-comments');

let inLoadingState = false;

let comments = [];

function updateRepliesToggleElement(element, areRepliesShown, replyCount) {
    if (replyCount > 0) {
        element.innerHTML = `${areRepliesShown ? 'Hide' : 'Show'} ${replyCount} ${replyCount !== 1 ? 'replies' : 'reply'}`;
    } else element.remove();
}

function addReplyButton(comment, commentFeatures, areRepliesShown, replyContainer) {
    let replyButton = document.createElement('a');
    replyButton.innerHTML = 'Reply';
    replyButton.addEventListener('click', function () {
        let replyBox = document.getElementById('reply-box');
        if (replyBox) {
            // first remove it from its place and then add it where it's called
            replyBox.remove();
        } else {
            // copy the comment box;
            replyBox = commentBox.cloneNode(true);
            replyBox.id = 'reply-box';
            let replyForm = replyBox.querySelector('form');
            replyForm.id = 'reply-form';
            let sbmtReplyBtn = replyBox.querySelector('div[class="button"]');
            sbmtReplyBtn.removeEventListener('click', onSubmit);
            sbmtReplyBtn.addEventListener('click', function () {
                //let reply =
                postComment("/api/comments/", "POST",
                    replyForm['comment-body'].value, comment.id)
                    .then(reply => {
                        // TODO: tell the user that the post isn't successful
                        if (!reply)
                            throw "Couldn't post reply";

                        let replyEl = createComment(reply, true);
                        replyContainer.appendChild(replyEl);
                        // ensure correct reply count in the scenario of replying after replies are shown
                        if (areRepliesShown) {
                            updateRepliesToggleElement(replyBox.previousElementSibling, areRepliesShown, comment.childCommentCount);
                        }
                        replyBox.remove();
                        replyForm['comment-body'].value = '';
                    });
            });
        }
        replyContainer.appendChild(replyBox);
    });
    commentFeatures.lastChild.insertBefore(replyButton, commentFeatures.lastChild.lastChild);
}

function addDeleteButton(comment, commentEl, commentOptionsEl) {
    let deleteButton = document.createElement('a');
    deleteButton.innerHTML = 'Delete';
    deleteButton.addEventListener('click', function () {
        let deleteConfirmed = confirm(`Delete comment "${(comment.text.length <= 50 ?
            comment.text : comment.text.substring(0, 80) + '...')}"` + `\nby user ${comment.authorName}?`);
        if (deleteConfirmed) {
            postComment(`/api/comments/delete/${comment.id}`, "DELETE",
                null, null)
                .then(() => {
                    // TODO: update child comments count
                    // updateRepliesToggleElement(parentCommentEl, areRepliesShown, parentComment)
                    commentEl.parentElement.removeChild(commentEl);
                });
        }
    });
    deleteButton.classList.add('is-pulled-right');
    commentOptionsEl.insertBefore(deleteButton, commentOptionsEl.lastChild);
}

// TODO: rename to "createOneComment"
function createComment(comment, isReply) {

    function toggleReplies(toggleRepliesBtn) {
        // cleaning the text content before showing replies
        // to cover the scenario of adding a reply and then showing the replies
        replyContainer.textContent = '';
        if (!areRepliesShown) {
            fetch(`/api/comments/${comment.id}/childComments`, {
                headers: {
                    'Content-type': 'application/json; charset=utf-8'
                }
            })
                .then(response => response.json())
                .then(comments => {
                    comments.forEach(cReply => {
                        let reply = createComment(cReply, true);
                        replyContainer.appendChild(reply);
                    });
                })
                .catch(error => {
                    // TODO: pop an error child comments not loaded
                    // commentSection.getElementsByTagName("h1")[0].innerHTML = 'Could not load comments.'
                    console.error('Unable to get comment replies.', error);
                })
                .then(() => {
                    inLoadingState = false;
                });
        }
        areRepliesShown = !areRepliesShown;
        updateRepliesToggleElement(toggleRepliesBtn, areRepliesShown, comment.childCommentCount);
    }

    let commentEl = commentTemplate.cloneNode(true);
    commentEl.removeAttribute('id');
    commentEl.removeAttribute("style");
    // commentEl.querySelector('img').src = comment.user.avatarSrc;

    let commentFeatures = commentEl.querySelector('p[name="comment-features"]');

    commentFeatures.innerHTML = `<strong>${comment.authorName}</strong>` +
        `<br>${comment.text}<br>` + `<small class="spaced">${comment.createdDate}</small>`;

    let commentOptionsEl = commentFeatures.lastChild;

    let replyContainer = commentEl.querySelector('div[id="replies-0"]');
    let areRepliesShown = false;

    if (isReply) {
        // arrange css
        commentEl.querySelector('img').parentElement.classList.replace('is-64x64', 'is-48x48');
        replyContainer.remove();
    } else {
        replyContainer.id = `replies-${comment.id}`;
        if (userIsLoggedIn) {
            addReplyButton(comment, commentFeatures, areRepliesShown, replyContainer);
        }
        if (comment.childCommentCount > 0) {
            // show replies button
            let toggleRepliesBtn = document.createElement('a');
            toggleRepliesBtn.outerHTML = '<a></a>';
            updateRepliesToggleElement(toggleRepliesBtn, areRepliesShown, comment.childCommentCount);
            toggleRepliesBtn.addEventListener('click', function () {
                toggleReplies(toggleRepliesBtn);
            });
            replyContainer.parentElement.insertBefore(toggleRepliesBtn, replyContainer.nextSibling);
        }
    }

    if (userIsLoggedIn && (comment.authorId === userId || userIsSuperUser)) {
        addDeleteButton(comment, commentEl, commentOptionsEl);
    }

    return commentEl;
}

function initializeComments() {
    inLoadingState = true;

    fetch(`/api/comments/article/${articleId}`, {
        headers: {
            'Content-type': 'application/json; charset=utf-8'
        }
    })
        .then(response => response.json())
        .then(comments => {
            comments.forEach(comment => {
                let commentEl = createComment(comment, false);
                commentsContainer.appendChild(commentEl);
            });
            if (comments.length === 0) {
                // TODO: display "No comments to display."
            }
            commentSection.getElementsByTagName("h1")[0].innerHTML = 'Comments';
            if (userIsLoggedIn) {
                commentBox.removeAttribute("style");
                commentBox.querySelector("div.button").addEventListener("click", onSubmit);
            } else {
                // TODO: display "Log in to leave a comment"
            }
        })
        // .catch(error => {
        //     commentSection.getElementsByTagName("h1")[0].innerHTML = 'Could not load comments.'
        //     console.error('Unable to get comments.', error);
        // })
        .then(() => {
            inLoadingState = false;
        });
}

let commentForm = document.forms['comment-form'];

async function postComment(path, methodType, input, parentCommentId) {
    let comment = {
        articleId,
        parentCommentId,
        text: input,
    }

    let responseComment = await fetch(path, {
        method: methodType,
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json; charset=utf-8',
            'X-CSRF-TOKEN': token
        },
        body: JSON.stringify(comment)
    });

    // TODO: use a cleaner approach here
    return methodType !== 'DELETE' ? responseComment.json() : null;
}

function onSubmit() {
    let input = commentForm['comment-body'].value;
    if (input.length < 2 || input.length > 550) {
        window.alert("Please enter a comment with length between 2 and 550 symbols!")
        return;
    }

    postComment("/api/comments/", "POST",
        input, null)
        .then(comment => {
            // in case we want to refresh all of the comments after submit
            // commentsContainer.innerHTML = '';
            // initializeComments();
            let commentEl = createComment(comment, false);
            // TODO: consider paging or limiting the amount of comments listed
            // if (commentsContainer.childElementCount >= maximumCommentsShown) {
            //     commentsContainer.lastElementChild.remove();
            // }
            commentsContainer.insertBefore(commentEl, commentsContainer.firstElementChild);
            commentForm['comment-body'].value = '';
        });
}

window.onload = initializeComments;
