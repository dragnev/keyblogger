// specific functionality for the AJAX modal in admin/categories
// contains methods for modal-actions.js

// var because these are referenced in modal-actions
var redirect = "/admin/users/";

// the generic method in admin/categories JS file can resolve all of these
let emailElement = modalForm["email"];
let fullNameElement = modalForm["fullName"];
let passwordElement = modalForm["password"];
let confirmPasswordElement = modalForm["confirmPassword"];

// but not this one
let rolesFieldsetElement = modal.querySelector('fieldset#roles-checkboxes');

function bindDataToForm(email, fullName, roles) {
    emailElement.value = email;
    fullNameElement.value = fullName;
    // set role checkboxes to unchecked
    rolesFieldsetElement.querySelectorAll('input[type=checkbox]').forEach(el => el.checked = false);
    // then check the roles of this user
    for (const roleId of roles) {
        rolesFieldsetElement.querySelector("input#role-" + roleId).checked = true;
    }
}

function toggleEditableState(enabled) {
    emailElement.disabled = !enabled;
    fullNameElement.disabled = !enabled;
    passwordElement.disabled = !enabled;
    confirmPasswordElement.disabled = !enabled;
    rolesFieldsetElement.disabled = !enabled;
    saveButton.style.display = enabled ? 'initial' : 'none';
    deleteButton.style.display = enabled ? 'none' : 'initial';
    if (enabled) {
        saveButton.style = 'padding: 0.85em 1em;';
    } else {
        deleteButton.style = 'padding: 0.85em 1em;';
    }

    // clear any errors left
    toggleFormError(false, "There are some errors in your form.");
    // override refresh on submit
    modalForm.addEventListener('submit', overrideDefaultSubmit);
}

// keep one parameter to have the same signature as the method in admin-categories
function bindDataToObject(data) {
    let selectedRoles = [];
    let rolesCheckboxes = rolesFieldsetElement.querySelectorAll('input[type=checkbox]:checked');

    for (let i = 0; i < rolesCheckboxes.length; i++) {
        selectedRoles.push(rolesCheckboxes[i].value)
    }
    let user = {
        email: emailElement.value,
        fullName: fullNameElement.value,
        password: passwordElement.value,
        confirmPassword: confirmPasswordElement.value,
        roles: selectedRoles
    };

    return user;
}

function onEdit(id, email, fullName, roles) {
    // item = { id, email, fullName, roles };
    bindDataToForm(email, fullName, roles);
    arrangeModal('Edit', "user",
        true, null,
        {type: "PUT", path: `/api/admin/users/edit/${id}`});
}

function onDelete(id, email, fullName, roles) {
    // item = { id, email, fullName, roles };
    bindDataToForm(email, fullName, roles);
    arrangeModal('Delete', "user",
        false, null,
        {type: "DELETE", path: `/api/admin/users/delete/${id}`});
}