var quill = new Quill('#editor-container', {
    modules: {
        formula: true,
        syntax: true,
        toolbar: '#toolbar-container'
    },
    placeholder: 'Enter article content here',
    theme: 'snow'
});

function getText(html) {
    let tmp = document.createElement('div');
    tmp.innerHTML = html;

    return tmp.textContent || tmp.innerText;
}

let form = document.getElementById("articleForm");
let myEditor = document.querySelector('#editor-container');
let contentElement = myEditor.children[0];

function overrideDefaultSubmit(event) { event.preventDefault(); }
form.addEventListener('submit', overrideDefaultSubmit);

let formAlertElement = form.querySelector('div[data-abide-error]');
let contentLabel = form.querySelector("label#contentLabel");
let spanError = contentLabel.querySelector("span.form-error");

let contentErrorIsShown = false;

function toggleContentLengthError(enabled) {
    if (enabled) {
        formAlertElement.style.display = 'initial';
        contentLabel.classList.add("is-invalid-label");
        spanError.classList.add("is-visible");
        contentErrorIsShown = true;
        document.getElementById("toolbar-container").classList.add("content-error");
        document.getElementById("editor-container").classList.add("content-error");
    } else {
        // TODO: don't hide it if form fields are invalid too (or don't show it in the first place)
        formAlertElement.style.display = 'none';
        contentLabel.classList.remove("is-invalid-label");
        spanError.classList.remove("is-visible");
        contentErrorIsShown = false;
        document.getElementById("toolbar-container").classList.remove("content-error");
        document.getElementById("editor-container").classList.remove("content-error");
    }
}

function validateContent() {
    if (getText(contentElement.innerHTML).length >= 10) {
        if (contentErrorIsShown) {
            toggleContentLengthError(false);
        }
    } else {
        if (!contentErrorIsShown) {
            toggleContentLengthError(true);
        }
    }
}

let contentIsObserved = false;

function contentIsValid() {
    let content = contentElement.innerHTML;

    if (getText(content).length < 10) {

        if (contentIsObserved === false) {
            // observe content innerHTML
            quill.on('text-change', function(delta, oldDelta, source) {
                validateContent();
            });
            validateContent();

            contentIsObserved = true;
        }
        return false;
    }
    return true;
}

const token = document.querySelector('meta[name="_csrf"]').getAttribute('content');
const header = document.querySelector('meta[name="_csrf_header"]').getAttribute('content');

function onSubmit() {
    let contntIsValid = contentIsValid();

    if (!contntIsValid) return false;

    if (!form["title"].value) return false;

    let article = {
        title: form["title"].value,
        content: contentElement.innerHTML,
        categoryId: form["categoryId"].value,
        tagString: form["tagString"].value
    }

    // ajax post
    let xhr = new XMLHttpRequest();
    xhr.open(request.type, request.path, true);

    // Send the proper header information along with the request
    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhr.setRequestHeader(header, token);
    // buttonLoadingState(true, button, null);

    xhr.onreadystatechange = function () { // Call a function when the state changes.
        // this.readyState === XMLHttpRequest.DONE &&
        if (this.status === 200) {
            // Request finished. Do processing here.
            window.location.href = `/article/${JSON.parse(this.response).id}`;
            // addMessageAndRedirect(button);
        } else {
            // On error
            // buttonLoadingState(false, button, actionLabel);
            // toggleFormError(true, "Bad response from the server.");
        }
    }

    xhr.send(JSON.stringify(article));

    return false;
}