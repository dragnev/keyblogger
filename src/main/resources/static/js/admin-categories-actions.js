// specific functionality for the AJAX modal in admin/categories
// contains methods for modal-actions.js

// var because these are referenced in modal-actions
var redirect = "/admin/categories/";
var nameElement = modalForm["name"];
let item = { id: null, name: null };


function bindDataToForm(data) {
    for (const [key, value] of Object.entries(data)) {
        modalForm[key].value = value;
    }
}

function toggleEditableState(enabled, action) {
    nameElement.disabled = !enabled;
    if (enabled) {
        saveButton.innerHTML = action;
        saveButton.style = 'padding: 0.85em 1em;';
        saveButton.style.display = 'initial';
        deleteButton.style.display = 'none';
    } else {
        deleteButton.innerHTML = 'Delete';
        deleteButton.style = 'padding: 0.85em 1em;';
        saveButton.style.display = 'none';
        deleteButton.style.display = 'initial';
    }
    // clear any errors left
    toggleFormError(false, "There are some errors in your form.");
    // override refresh on clicking submit
    modalForm.addEventListener('submit', overrideDefaultSubmit);
}

function bindDataToObject(data) {
    let item = {};
    for (const [key, value] of Object.entries(data)) {
        item[key] = modalForm[key].value;
    }
    return item;
}

function onCreate() {
    item.name = null;
    bindDataToForm(item);
    arrangeModal('Create', "category",
        true, item,
        { type: "POST", path: `/api/admin/categories/create` });
}

function onEdit(id, name) {
    item = { id, name };
    bindDataToForm(item);
    arrangeModal('Edit', "category",
        true, item,
        { type: "PUT", path: `/api/admin/categories/edit/${id}` });
}

function onDelete(id, name) {
    item = { id, name };
    bindDataToForm(item);
    arrangeModal('Delete', "category",
        false, null,
        { type: "DELETE", path: `/api/admin/categories/delete/${id}` });
}
