package com.dragnev.keyblogger.constants;

public class SampleData {
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";

    public static final String ADMIN_USER_EMAIL = "admin@admin.com";
    public static final String ADMIN_USER_NAME = "Admin";
    public static final String ADMIN_USER_PASSWORD = "KBReGW8htdGfeGq";

    public static final String USER_1_EMAIL = "casedeg@gmail.com";
    public static final String USER_1_USER_NAME = "Cees de Groot";
    public static final String USER_1_PASSWORD = "evEzyFZU";

    public static final String USER_2_EMAIL = "stan@twinb.com";
    public static final String USER_2_USER_NAME = "McLaren Stanley";
    public static final String USER_2_PASSWORD = "37mHap4C";

    public static final String USER_3_EMAIL = "jfrederik@thecorrespondent.com";
    public static final String USER_3_USER_NAME = "Jesse Frederik";
    public static final String USER_3_PASSWORD = "W8m95xf7";

    public static final String USER_4_EMAIL = "gcorfield@theregister.com";
    public static final String USER_4_USER_NAME = "Gareth Corfield";
    public static final String USER_4_PASSWORD = "RBF3cq5r";

    public static final String USER_5_EMAIL = "kc@qz.com";
    public static final String USER_5_USER_NAME = "Keith Collins";
    public static final String USER_5_PASSWORD = "csFzgMC2";

    public static final String CATEGORY_1 = "Programming";
    public static final String CATEGORY_2 = "Technology";

    public static final String TAG_1 = "Uber";
    public static final String TAG_2 = "Blockchain";
    public static final String TAG_3 = "Code";
    public static final String TAG_4 = "Planes";
    public static final String TAG_5 = "Bugs";
    public static final String TAG_6 = "NASA";
    public static final String TAG_7 = "Apollo";
    public static final String TAG_8 = "Serverless";
    public static final String TAG_9 = "Moon";
    public static final String TAG_10 = "Disaster";

    public static final String ARTICLE_1_HEADING = "Back to the '70s with Serverless";
    public static final String ARTICLE_1_CONTENT_PATH = "./src/main/resources/sampleData/article-1.html";

    public static final String ARTICLE_2_HEADING = "Former Uber engineer shares “biggest engineering disaster” story";
    public static final String ARTICLE_2_CONTENT_PATH = "./src/main/resources/sampleData/article-2.html";

    public static final String ARTICLE_3_HEADING = "Blockchain, the amazing solution for almost nothing";
    public static final String ARTICLE_3_CONTENT_PATH = "./src/main/resources/sampleData/article-3.html";

    public static final String ARTICLE_4_HEADING = "Boeing 787s must be turned off and on every 51 days to prevent 'misleading data' being shown to pilots";
    public static final String ARTICLE_4_CONTENT_PATH = "./src/main/resources/sampleData/article-4.html";

    public static final String ARTICLE_5_HEADING = "The code that took America to the moon was just published to GitHub, and it's like a 1960s time capsule";
    public static final String ARTICLE_5_CONTENT_PATH = "./src/main/resources/sampleData/article-5.html";
}
