package com.dragnev.keyblogger.applicationEvent;

import com.dragnev.keyblogger.entity.*;
import com.dragnev.keyblogger.repository.*;
import com.dragnev.keyblogger.utils.IO.ReadFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;

import static com.dragnev.keyblogger.constants.SampleData.*;

@Component
public class DbContextRefreshListener {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private ArticleRepository articleRepository;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        var admin = userRepository.findByEmail(ADMIN_USER_EMAIL);
        if (admin == null) {

            var userRole = new Role(ROLE_USER);
            var adminRole = new Role(ROLE_ADMIN);
            roleRepository.save(userRole);
            roleRepository.save(adminRole);

            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            // admin account
            userRepository.save(new User(ADMIN_USER_EMAIL, ADMIN_USER_NAME, bCryptPasswordEncoder.encode(ADMIN_USER_PASSWORD), adminRole));

            // authors
            var authorCasedeg = new User(USER_1_EMAIL, USER_1_USER_NAME, bCryptPasswordEncoder.encode(USER_1_PASSWORD), userRole);
            var authorStan = new User(USER_2_EMAIL, USER_2_USER_NAME, bCryptPasswordEncoder.encode(USER_2_PASSWORD), userRole);
            var authorJFred = new User(USER_3_EMAIL, USER_3_USER_NAME, bCryptPasswordEncoder.encode(USER_3_PASSWORD), userRole);
            var authorGCorf = new User(USER_4_EMAIL, USER_4_USER_NAME, bCryptPasswordEncoder.encode(USER_4_PASSWORD), userRole);
            var authorKC = new User(USER_5_EMAIL, USER_5_USER_NAME, bCryptPasswordEncoder.encode(USER_5_PASSWORD), userRole);
            userRepository.save(authorCasedeg);
            userRepository.save(authorStan);
            userRepository.save(authorJFred);
            userRepository.save(authorGCorf);
            userRepository.save(authorKC);

            // categories
            var categProgramming = new Category(CATEGORY_1);
            var categTechnology = new Category(CATEGORY_2);
            categoryRepository.save(categProgramming);
            categoryRepository.save(categTechnology);

            // tags
            var tagUber = new Tag(TAG_1);
            var tagBlockchain = new Tag(TAG_2);
            var tagCode = new Tag(TAG_3);
            var tagPlanes = new Tag(TAG_4);
            var tagBugs = new Tag(TAG_5);
            var tagNasa = new Tag(TAG_6);
            var tagApollo = new Tag(TAG_7);
            var tagServerless = new Tag(TAG_8);
            var tagMoon = new Tag(TAG_9);
            var tagDisaster = new Tag(TAG_10);
            tagRepository.save(tagUber);
            tagRepository.save(tagBlockchain);
            tagRepository.save(tagCode);
            tagRepository.save(tagPlanes);
            tagRepository.save(tagPlanes);
            tagRepository.save(tagBugs);
            tagRepository.save(tagNasa);
            tagRepository.save(tagApollo);
            tagRepository.save(tagServerless);
            tagRepository.save(tagMoon);
            tagRepository.save(tagDisaster);

            // articles
            try {
                articleRepository.save(new Article(ARTICLE_1_HEADING, ReadFile.readFile(ARTICLE_1_CONTENT_PATH, StandardCharsets.UTF_8), authorCasedeg, categProgramming, new HashSet<>(Arrays.asList(tagServerless))));
                articleRepository.save(new Article(ARTICLE_2_HEADING, ReadFile.readFile(ARTICLE_2_CONTENT_PATH, StandardCharsets.UTF_8), authorStan, categProgramming, new HashSet<>(Arrays.asList(tagUber, tagDisaster))));
                articleRepository.save(new Article(ARTICLE_3_HEADING, ReadFile.readFile(ARTICLE_3_CONTENT_PATH, StandardCharsets.UTF_8), authorJFred, categTechnology, new HashSet<>(Arrays.asList(tagBlockchain))));
                articleRepository.save(new Article(ARTICLE_4_HEADING, ReadFile.readFile(ARTICLE_4_CONTENT_PATH, StandardCharsets.UTF_8), authorGCorf, categProgramming, new HashSet<>(Arrays.asList(tagCode, tagPlanes, tagBugs))));
                articleRepository.save(new Article(ARTICLE_5_HEADING, ReadFile.readFile(ARTICLE_5_CONTENT_PATH, StandardCharsets.UTF_8), authorKC, categProgramming, new HashSet<>(Arrays.asList(tagCode, tagNasa, tagApollo, tagMoon))));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
