package com.dragnev.keyblogger.utils.mapper;

import com.dragnev.keyblogger.bindingModel.CommentBindingModel;
import com.dragnev.keyblogger.entity.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentBindingModelMapper {
    public static CommentBindingModel mapCommentToBindingModel(Comment comment) {
        var model = new CommentBindingModel() {{
            setId(comment.getId());
            setText(comment.getText());
            setCreatedDate(comment.getCreatedDate());
            setArticleId(comment.getArticle().getId());
            setAuthorId(comment.getUser().getId());
            setAuthorName(comment.getUser().getFullName());
            setHidden(comment.getIsHidden());
            setChildCommentCount(comment.getChildComments().stream().count());
        }};

        return model;
    };

    public static List<CommentBindingModel> mapCommentsToBindingModelList(List<Comment> comments) {
        var model = new ArrayList<CommentBindingModel>();

        for (var comment : comments) {
            model.add(new CommentBindingModel() {{
                setId(comment.getId());
                setText(comment.getText());
                setCreatedDate(comment.getCreatedDate());
                setArticleId(comment.getArticle().getId());
                setAuthorId(comment.getUser().getId());
                setAuthorName(comment.getUser().getFullName());
                setHidden(comment.getIsHidden());
                setChildCommentCount(comment.getChildComments().stream().count());
            }});
        }
        return model;
    }
}

