package com.dragnev.keyblogger.repository;

import com.dragnev.keyblogger.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}
