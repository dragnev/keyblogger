package com.dragnev.keyblogger.repository;
import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    // user methods
    @Query("select c from Comment c where c.article = ?1 and c.isHidden = false and c.parentComment is null order by c.createdDate DESC")
    List<Comment> findByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc(Article article);

    @Query("select c from Comment c where c.parentComment = ?1 and c.isHidden = false order by c.createdDate")
    List<Comment> findByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc(Comment parentComment);

    // admin methods
    @Query("select c from Comment c where c.article = ?1 order by c.createdDate DESC")
    List<Comment> findByArticleOrderByCreatedDateDesc(Article article);

    @Query("select c from Comment c where c.parentComment = ?1 order by c.createdDate")
    List<Comment> findByParentCommentOrderByCreatedDateAsc(Comment parentComment);
}
