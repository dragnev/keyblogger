package com.dragnev.keyblogger.repository;
import com.dragnev.keyblogger.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String name);

    @Query("select u from User u where u.articles is not empty")
    List<User> findByArticlesIsNotEmpty();
}
