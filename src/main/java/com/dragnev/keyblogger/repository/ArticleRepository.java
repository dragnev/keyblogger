package com.dragnev.keyblogger.repository;
import com.dragnev.keyblogger.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
}
