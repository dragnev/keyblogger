package com.dragnev.keyblogger.repository;

import com.dragnev.keyblogger.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    Tag findByName(String name);
    boolean existsByName(String name);
    List<Tag> findByArticlesIsNotEmpty();
}
