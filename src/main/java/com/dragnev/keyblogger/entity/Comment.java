package com.dragnev.keyblogger.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "comments")
public class Comment {
    public Comment() {
    }

    public Comment(User user, Article article, String text) {
        this.user = user;
        this.article = article;
        this.text = text;
        this.isHidden = false;
        this.childComments = new ArrayList<>();
    }

    public Comment(User user, Article article, Comment parentComment, String text) {
        this.user = user;
        this.article = article;
        this.parentComment = parentComment;
        this.text = text;
        this.isHidden = false;
        this.childComments = new ArrayList<>();
    }

    private Integer id;
    private User user;
    private Article article;
    private Comment parentComment;
    private Timestamp createdDate;
    private String text;
    private Boolean isHidden;
    private List<Comment> childComments;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @ManyToOne()
    @JoinColumn(name = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "article_id")
    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @ManyToOne
    @JoinColumn(name = "parent_comment_id")
    public Comment getParentComment() {
        return parentComment;
    }

    public void setParentComment(Comment parentComment) {
        this.parentComment = parentComment;
    }

    @CreationTimestamp
    @Column(name = "createdDate")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "text", nullable = false)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "is_hidden", columnDefinition="BOOLEAN DEFAULT false")
    public Boolean getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(Boolean isHidden) {
        this.isHidden = isHidden;
    }

    @OneToMany(mappedBy = "parentComment", orphanRemoval = true)
    public List<Comment> getChildComments() {
        return childComments;
    }

    public void setChildComments(List<Comment> childComments) {
        this.childComments = childComments;
    }
}
