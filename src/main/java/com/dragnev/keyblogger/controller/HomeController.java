package com.dragnev.keyblogger.controller;

import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.Category;
import com.dragnev.keyblogger.entity.Tag;
import com.dragnev.keyblogger.entity.User;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CategoryRepository;
import com.dragnev.keyblogger.repository.TagRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Objects;

@Controller
public class HomeController {
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TagRepository tagRepository;

    @GetMapping("/")
    public String index(Model model, @RequestParam(name = "categoryId", required = false) Integer categoryId,
                        @RequestParam(name = "userId", required = false) Integer userId,
                        @RequestParam(name = "tag", required = false) String tagName){
        var articles = articleRepository.findAll();

        Category category = null;
        User user = null;
        Tag tag = null;

        if (categoryId != null && categoryRepository.existsById(categoryId)) {
            // TODO: use queryDSL or whatever for performance reasons
            category = categoryRepository.getById(categoryId);
            articles.removeIf(a -> !Objects.equals(a.getCategory().getId(), categoryId));
        }
        if (userId != null && userRepository.existsById(userId)) {
            user = userRepository.getById(userId);
            articles.removeIf(a -> !Objects.equals(a.getAuthor().getId(), userId));
        }
        if (tagName != null && tagRepository.existsByName(tagName)) {
            tag = tagRepository.findByName(tagName);

            // this because of compile-time error
            Tag finalTag = tag;

            articles.removeIf(a -> !a.getTags().contains(finalTag));
        }
        articles = setArticleContentToPureText(articles);
        var users = user == null ? userRepository.findByArticlesIsNotEmpty() : null;
        var categories = category == null ? categoryRepository.findByArticlesIsNotEmpty() : null;
        var tags = tag == null ? tagRepository.findByArticlesIsNotEmpty() : null;

        model.addAttribute("selectedCategory", category);
        model.addAttribute("selectedAuthor", user);
        model.addAttribute("selectedTag", tag);

        model.addAttribute("view", "home/index");
        model.addAttribute("articles", articles);
        model.addAttribute("categories", categories);
        model.addAttribute("users", users);
        model.addAttribute("tags", tags);
        return "base-layout";
    }

    private static List<Article> setArticleContentToPureText(List<Article> articles) {
        // TODO: consider getting substring of the content here instead of inside the view
        for (var article: articles) {
            // TODO: preserve newlines
            // var jsoupDoc =
            article.setContent(Jsoup.parse(article.getContent()).text());
        }
        return articles;
    }

    @GetMapping("/error/403")
    public String accessDenied(Model model){
        model.addAttribute("view", "error/403");
        return "base-layout";
    }


}
