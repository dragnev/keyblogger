package com.dragnev.keyblogger.controller;

import com.dragnev.keyblogger.bindingModel.ArticleBindingModel;
import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.Tag;
import com.dragnev.keyblogger.entity.User;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CategoryRepository;
import com.dragnev.keyblogger.repository.TagRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.stream.Collectors;

@Controller
public class ArticleController {
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TagRepository tagRepository;

    @GetMapping("article/create")
    @PreAuthorize("isAuthenticated()")
    public String create(Model model) {
        var categories = categoryRepository.findAll();

        model.addAttribute("view", "article/create");
        model.addAttribute("categories", categories);

        return "base-layout";
    }

    @GetMapping("/article/{id}")
    public String details(Model model, @PathVariable Integer id) {
        if (!articleRepository.existsById(id)){
            return "redirect:/";
        }

        if (!(SecurityContextHolder.getContext().getAuthentication()
                instanceof AnonymousAuthenticationToken)) {
            UserDetails principal = (UserDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
            User entityUser = userRepository.findByEmail(principal.getUsername());
            model.addAttribute("user", entityUser);
        }

        var article = articleRepository.findById(id).orElse(null);
        model.addAttribute("article", article);
        model.addAttribute("view", "article/details");

        return "base-layout";
    }

    @GetMapping("/article/edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
        if (!articleRepository.existsById(id)){
            return "redirect:/";
        }

        var article = articleRepository.findById(id).orElse(null);

        if (!isUserAuthorOrAdmin(article)) {
            return "redirect:/article/" + id;
        }

        var categories = categoryRepository.findAll();
        var tagString = article.getTags().stream()
                        .map(Tag::getName)
                                .collect(Collectors.joining(", "));

        model.addAttribute("article", article);
        model.addAttribute("categories", categories);
        model.addAttribute("tags", tagString);
        model.addAttribute("view", "article/edit");

        return "base-layout";
    }

    @PostMapping("/article/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String editProcess(@PathVariable Integer id, ArticleBindingModel articleBindingModel) {
        if (!articleRepository.existsById(id)){
            return "redirect:/";
        }

        var article = articleRepository.findById(id).orElse(null);
        if (!isUserAuthorOrAdmin(article)) {
            return "redirect:/article/" + id;
        }

        var category = categoryRepository.getById(articleBindingModel.getCategoryId());
        var tags = findTagsFromString(articleBindingModel.getTagString());

        article.setTitle(articleBindingModel.getTitle());
        article.setContent(articleBindingModel.getContent());
        article.setCategory(category);
        article.setTags(tags);

        articleRepository.saveAndFlush(article);

        return "redirect:/article/" + article.getId();
    }

    @PostMapping(value = "/article/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String deleteProcess(@PathVariable Integer id) {
        if (!articleRepository.existsById(id)){
            return "redirect:/";
        }

        var article = articleRepository.findById(id).orElse(null);

        if (!isUserAuthorOrAdmin(article)) {
            return "redirect:/article/" + id;
        }

        articleRepository.delete(article);

        return "redirect:/";
    }

    private boolean isUserAuthorOrAdmin(Article article) {
        var user = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        var userEntity = userRepository.findByEmail(user.getUsername());
        return userEntity.isAdmin() || userEntity.isAuthor(article);
    }

    private HashSet<Tag> findTagsFromString(String tagString) {
        HashSet<Tag> tags = new HashSet<>();
        var tagNames = tagString.split(",\\s*");
        for (var tagName : tagNames) {
            var currentTag = tagRepository.findByName(tagName);

            if (currentTag == null) {
                currentTag = new Tag(tagName);
                tagRepository.saveAndFlush(currentTag);
            }

            tags.add(currentTag);
        }
        return tags;
    }
}
