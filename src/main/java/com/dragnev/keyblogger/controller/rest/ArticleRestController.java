package com.dragnev.keyblogger.controller.rest;

import com.dragnev.keyblogger.bindingModel.ArticleBindingModel;
import com.dragnev.keyblogger.entity.Article;
import com.dragnev.keyblogger.entity.Tag;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CategoryRepository;
import com.dragnev.keyblogger.repository.TagRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;

@RestController
@RequestMapping("/api/articles")
public class ArticleRestController {
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TagRepository tagRepository;

    @RequestMapping(value = "/create", produces = "application/json",
            consumes = "application/json", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Article> createProcess(@RequestBody ArticleBindingModel articleBindingModel) {
        var principal = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        var userEntity = userRepository.findByEmail(principal.getUsername());
        var category = categoryRepository.getById(articleBindingModel.getCategoryId());
        var tags = findTagsFromString(articleBindingModel.getTagString());

        var articleEntity = new Article(
                articleBindingModel.getTitle(),
                articleBindingModel.getContent(),
                userEntity,
                category,
                tags);
        articleRepository.saveAndFlush(articleEntity);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/edit/{id}", produces = "application/json",
            consumes = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<Article> editProcess(@PathVariable Integer id,
                                                @RequestBody ArticleBindingModel articleBindingModel) {
        if (!articleRepository.existsById(id)){
            return ResponseEntity.badRequest().build();
        }

        var article = articleRepository.findById(id).orElse(null);
        if (!isUserAuthorOrAdmin(article)) {
            return ResponseEntity.badRequest().build();
        }

        var category = categoryRepository.getById(articleBindingModel.getCategoryId());
        var tags = findTagsFromString(articleBindingModel.getTagString());

        article.setTitle(articleBindingModel.getTitle());
        article.setContent(articleBindingModel.getContent());
        article.setCategory(category);
        article.setTags(tags);

        articleRepository.saveAndFlush(article);

        // entity needs to be remapped first to avoid circular references
        // but all I need is the ID so bear with me...
        var response = new Article() {{ setId(article.getId()); setContent(article.getContent()); }};

        return ResponseEntity.ok(response);
    }

    private boolean isUserAuthorOrAdmin(Article article) {
        var user = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        var userEntity = userRepository.findByEmail(user.getUsername());
        return userEntity.isAdmin() || userEntity.isAuthor(article);
    }

    private HashSet<Tag> findTagsFromString(String tagString) {
        HashSet<Tag> tags = new HashSet<>();
        var tagNames = tagString.split(",\\s*");
        for (var tagName : tagNames) {
            var currentTag = tagRepository.findByName(tagName);

            if (currentTag == null) {
                currentTag = new Tag(tagName);
                tagRepository.saveAndFlush(currentTag);
            }

            tags.add(currentTag);
        }
        return tags;
    }
}
