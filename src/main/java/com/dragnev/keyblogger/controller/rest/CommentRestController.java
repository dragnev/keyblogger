package com.dragnev.keyblogger.controller.rest;

import com.dragnev.keyblogger.bindingModel.CommentBindingModel;
import com.dragnev.keyblogger.entity.Comment;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CommentRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import com.dragnev.keyblogger.utils.mapper.CommentBindingModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/article/{id}", produces = "application/json",
            method = RequestMethod.GET)
    public ResponseEntity<List<CommentBindingModel>> getByArticle(@PathVariable Integer id) {
        if (!articleRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }

        var article = articleRepository.getById(id);
        var comments =
                CommentBindingModelMapper.mapCommentsToBindingModelList(
                        commentRepository.findByArticleAndIsHiddenIsFalseAndParentCommentIsNullOrderByCreatedDateDesc(article));

        return ResponseEntity.ok(comments);
    }

    @RequestMapping(value = "/{id}/childComments", produces = "application/json",
            method = RequestMethod.GET)
    public ResponseEntity<List<CommentBindingModel>> getChildrenByParentComment(@PathVariable Integer id) {
        if (!commentRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }

        var parentComment = commentRepository.getById(id);
        if (parentComment.getArticle() == null) {
            // not a top-level comment
            return ResponseEntity.badRequest().build();
        }

        var comments =
                CommentBindingModelMapper.mapCommentsToBindingModelList(
                        commentRepository.findByParentCommentAndIsHiddenIsFalseOrderByCreatedDateAsc(parentComment));

        return ResponseEntity.ok(comments);
    }

    @RequestMapping(value = "/", produces = "application/json",
            consumes = "application/json", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<CommentBindingModel> createProcess(@RequestBody CommentBindingModel commentBindingModel) {
        if (commentBindingModel == null || StringUtils.isEmpty(commentBindingModel.getText())) {
            return ResponseEntity.badRequest().build();
        }

        if ((commentBindingModel.getArticleId() == null ||
                !articleRepository.existsById(commentBindingModel.getArticleId()))
                && (commentBindingModel.getParentCommentId() == null ||
                !commentRepository.existsById(commentBindingModel.getParentCommentId()))) {
            return ResponseEntity.badRequest().build();
        }

        var principal = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        var user = userRepository.findByEmail(principal.getUsername());

        var comment = commentBindingModel.getParentCommentId() == null ?
                // is top level
                new Comment(user,
                        articleRepository.getById(commentBindingModel.getArticleId()),
                        commentBindingModel.getText()) :
                // is child
                new Comment(user,
                        articleRepository.getById(commentBindingModel.getArticleId()),
                        commentRepository.getById(commentBindingModel.getParentCommentId()),
                        commentBindingModel.getText());

        commentRepository.saveAndFlush(comment);

        return ResponseEntity.ok(
                CommentBindingModelMapper.mapCommentToBindingModel(comment)
        );
    }

    @RequestMapping(value = "/edit/{id}", produces = "application/json",
            consumes = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<Comment> editProcess(@PathVariable Integer id,
                                                @RequestBody CommentBindingModel commentBindingModel) {
        if (!commentRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }
        if (StringUtils.isEmpty(commentBindingModel.getText())) {
            return ResponseEntity.badRequest().build();
        }

        Comment comment = commentRepository.getById(id);

        if (!isUserAuthorOrAdmin(comment)) {
            return ResponseEntity.badRequest().build();
        }

        comment.setText(commentBindingModel.getText());
        commentRepository.saveAndFlush(comment);
        return ResponseEntity.ok(comment);
    }

    @RequestMapping(value = {"/delete/{id}"}, produces = "application/json",
            consumes = "application/json", method = RequestMethod.DELETE)
    public ResponseEntity<Comment> deleteProcess(@PathVariable(name = "id") Integer id) {
        if (!commentRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }

        var comment = commentRepository.getById(id);

        if (!isUserAuthorOrAdmin(comment)) {
            return ResponseEntity.badRequest().build();
        }

        commentRepository.delete(comment);

        return ResponseEntity.ok().build();
    }

    private boolean isUserAuthorOrAdmin(Comment comment) {
        var user = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        var userEntity = userRepository.findByEmail(user.getUsername());
        return userEntity.isAdmin() || userEntity.isAuthor(comment);
    }
}
