package com.dragnev.keyblogger.controller.admin.rest;

import com.dragnev.keyblogger.bindingModel.CategoryBindingModel;
import com.dragnev.keyblogger.entity.Category;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.util.*;

@RestController
@RequestMapping("/api/admin/categories")
public class CategoryRestController {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @RequestMapping(value = "/", produces = "application/json",
            method = RequestMethod.GET)
    public ResponseEntity<List<Category>> get() {

        var categories = new ArrayList<Category>();

        for (var category : categoryRepository.findAll()) {
            categories.add(new Category() {{ setId(category.getId()); setName(category.getName());}});
        }

        categories.sort(Comparator.comparingInt(Category::getId));
        return ResponseEntity.ok(categories);
    }

    @RequestMapping(value = "/create", produces = "application/json",
            consumes = "application/json", method = RequestMethod.POST)
    public ResponseEntity<Category> createProcess(@RequestBody CategoryBindingModel categoryBindingModel) {
        if (StringUtils.isEmpty(categoryBindingModel.getName())) {
            return ResponseEntity.badRequest().build();
        }
        Category category = new Category(categoryBindingModel.getName());
        categoryRepository.saveAndFlush(category);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/edit/{id}", produces = "application/json",
            consumes = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<Category> editProcess(@PathVariable Integer id,
            @RequestBody CategoryBindingModel categoryBindingModel) {
        if (!categoryRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }
        if (StringUtils.isEmpty(categoryBindingModel.getName())) {
            return ResponseEntity.badRequest().build();
        }
        Category category = categoryRepository.getById(id);
        category.setName(categoryBindingModel.getName());
        categoryRepository.saveAndFlush(category);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = {"/delete/{id}"})
    public ResponseEntity<Category> deleteProcess(@PathVariable(name = "id", required = false) Integer id) {
        if (!categoryRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }

        var category = categoryRepository.getById(id);

        articleRepository.deleteAll(category.getArticles());

        categoryRepository.delete(category);

        return ResponseEntity.ok().build();
    }
}
