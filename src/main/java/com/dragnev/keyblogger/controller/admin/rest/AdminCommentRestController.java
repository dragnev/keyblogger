package com.dragnev.keyblogger.controller.admin.rest;

import com.dragnev.keyblogger.entity.Comment;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.CommentRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/admin/comments")
public class AdminCommentRestController {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ArticleRepository articleRepository;

    @RequestMapping(value = "/article/{id}", produces = "application/json",
            method = RequestMethod.GET)
    public ResponseEntity<List<Comment>> getByArticle(@PathVariable Integer id) {
        if (!articleRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }

        var article = articleRepository.getById(id);
        var comments = new ArrayList<Comment>();

        for (var comment : commentRepository.findByArticleOrderByCreatedDateDesc(article)) {
            var childComments = new ArrayList<Comment>();

            // we just need to get the count
            for (var ignored : comment.getChildComments()) {
                childComments.add(new Comment());
            }
            comments.add(new Comment() {{
                setId(comment.getId());
                setText(comment.getText());
                setCreatedDate(comment.getCreatedDate());
                setArticle(comment.getArticle());
                setUser(comment.getUser());
                setIsHidden(comment.getIsHidden());
                setChildComments(childComments);
            }});
        }

        return ResponseEntity.ok(comments);
    }

    @RequestMapping(value = "/{id}/childComments", produces = "application/json",
            method = RequestMethod.GET)
    public ResponseEntity<List<Comment>> getChildrenByParentComment(@PathVariable Integer id) {
        if (!commentRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }

        var parentComment = commentRepository.getById(id);
        if (parentComment.getArticle() == null) {
            // not a top-level comment
            return ResponseEntity.badRequest().build();
        }

        var comments = new ArrayList<Comment>();

        for (var comment : commentRepository.findByParentCommentOrderByCreatedDateAsc(parentComment)) {
            comments.add(new Comment() {{
                setId(comment.getId());
                setText(comment.getText());
                setCreatedDate(comment.getCreatedDate());
                setArticle(comment.getArticle());
                setUser(comment.getUser());
                setIsHidden(comment.getIsHidden());
            }});
        }

        return ResponseEntity.ok(comments);
    }
}
