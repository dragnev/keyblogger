package com.dragnev.keyblogger.controller.admin.rest;

import com.dragnev.keyblogger.bindingModel.UserEditBindingModel;
import com.dragnev.keyblogger.entity.Role;
import com.dragnev.keyblogger.entity.User;
import com.dragnev.keyblogger.repository.ArticleRepository;
import com.dragnev.keyblogger.repository.RoleRepository;
import com.dragnev.keyblogger.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/admin/users")
public class AdminUserRestController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(value = "/edit/{id}", produces = "application/json",
            consumes = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<User> editProcess(@PathVariable Integer id,
                                      @RequestBody UserEditBindingModel userBindingModel) {
        if (!this.userRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }
        User user = userRepository.getById(id);

        if (!StringUtils.isEmpty(userBindingModel.getPassword())
                && !StringUtils.isEmpty(userBindingModel.getConfirmPassword())) {
            if (userBindingModel.getPassword().equals(userBindingModel.getConfirmPassword())) {
                BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

                user.setPassword(bCryptPasswordEncoder.encode(userBindingModel.getPassword()));
            }
        }

        user.setFullName(userBindingModel.getFullName());
        user.setEmail(userBindingModel.getEmail());

        Set<Role> roles = new HashSet<>();

        for (Integer roleId : userBindingModel.getRoles()) {
            roles.add(roleRepository.getById(roleId));
        }

        user.setRoles(roles);

        userRepository.saveAndFlush(user);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
    public ResponseEntity<User> deleteTest(@PathVariable Integer id) {
        if (!this.userRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }

        User user = userRepository.getById(id);

        articleRepository.deleteAll(user.getArticles());

        userRepository.delete(user);

        return ResponseEntity.ok().build();
    }
}
