package com.dragnev.keyblogger.bindingModel;

import com.sun.istack.NotNull;

import java.sql.Timestamp;

public class CommentBindingModel {
    private Integer id;
    
    @NotNull
    private String text;

    @NotNull
    private Integer authorId;

    private String authorName;

    private Integer articleId;

    private Integer parentCommentId;

    private Timestamp createdDate;

    private Long childCommentCount;

    private Boolean isHidden;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Integer getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(Integer parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Long getChildCommentCount() {
        return childCommentCount;
    }

    public void setChildCommentCount(Long childCommentCount) {
        this.childCommentCount = childCommentCount;
    }

    public Boolean isHidden() {
        return isHidden;
    }

    public void setHidden(Boolean hidden) {
        isHidden = hidden;
    }
}
